###############################################################################
## @file classes/LINUX/rules-linux.mk
## @author Y.M. Morgan
## @date 2016/03/20
##
## Rules for LINUX modules.
###############################################################################

_module_msg := $(if $(_mode_host),Host )Linux

include $(BUILD_SYSTEM)/classes/GENERIC/rules.mk

###############################################################################
###############################################################################
LINUX_BUILD_DIR := $(call local-get-build-dir)

# Make sure this variable is defined (so make --warn-undefined-variables is quiet)
# It can be defined by the user makefile to specify a list of headers to be
# copied from linux source tree (list of absolute path)
# They will be copied in $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/include/linux
ifndef LINUX_EXPORTED_HEADERS
  LINUX_EXPORTED_HEADERS :=
endif

# Linux image to generate
ifndef TARGET_LINUX_IMAGE
  ifeq ("$(TARGET_LINUX_GENERATE_UIMAGE)","1")
    LOCAL_LINUX_IMAGE := uImage
  else ifeq ("$(LOCAL_LINUX_ARCH)","x86")
    LOCAL_LINUX_IMAGE := bzImage
  else ifeq ("$(LOCAL_LINUX_ARCH)","x64")
    LOCAL_LINUX_IMAGE := bzImage
  else ifeq ("$(LOCAL_LINUX_ARCH)","arm")
    LOCAL_LINUX_IMAGE := zImage
  else ifeq ("$(LOCAL_LINUX_ARCH)","arm64")
    LOCAL_LINUX_IMAGE := Image
  else ifeq ("$(LOCAL_LINUX_ARCH)","riscv")
    LOCAL_LINUX_IMAGE := Image
  else
    LOCAL_LINUX_IMAGE := zImage
  endif
else
    LOCAL_LINUX_IMAGE := $(TARGET_LINUX_IMAGE)
endif

###############################################################################
###############################################################################

# Linux configuration file or target
local_module := $(LOCAL_MODULE)
ifdef SUBTARGET
	local_module = $(LOCAL_MODULE).$(SUBTARGET)
endif
ifndef LINUX_CONFIG_FILE
	# we get both sublinux and LOCAL_MODULE and use the first as the winner
	LINUX_CONFIG_FILE := $(firstword $(wildcard $(call module-get-config,$(local_module)) $(call module-get-config,$(LOCAL_MODULE))))
endif
LOCAL_LINUX_CONFIG_FILE_IS_TARGET := $(false)
ifeq ("$(wildcard $(LINUX_CONFIG_FILE))","")
  ifdef LOCAL_LINUX_CONFIG_TARGET
    LOCAL_LINUX_CONFIG_FILE := $(LINUX_DIR)/arch/$(LOCAL_LINUX_SRCARCH)/configs/$(LOCAL_LINUX_CONFIG_TARGET)
    LOCAL_LINUX_CONFIG_FILE_IS_TARGET := $(true)
  else ifdef LINUX_CONFIG_TARGET
    LOCAL_LINUX_CONFIG_TARGET := $(LINUX_CONFIG_TARGET)
    LOCAL_LINUX_CONFIG_FILE := $(LINUX_DIR)/arch/$(LOCAL_LINUX_SRCARCH)/configs/$(LOCAL_LINUX_CONFIG_TARGET)
    LOCAL_LINUX_CONFIG_FILE_IS_TARGET := $(true)
  else ifdef LINUX_DEFAULT_CONFIG_TARGET
    LOCAL_LINUX_CONFIG_TARGET := $(LINUX_DEFAULT_CONFIG_TARGET)
    LOCAL_LINUX_CONFIG_FILE := $(LINUX_DIR)/arch/$(LOCAL_LINUX_SRCARCH)/configs/$(LOCAL_LINUX_CONFIG_TARGET)
    LOCAL_LINUX_CONFIG_FILE_IS_TARGET := $(true)
  else
    ifeq ("$(wildcard $(LINUX_DEFAULT_CONFIG_FILE))","")
      $(error No linux config file found default $(LINUX_DEFAULT_CONFIG_FILE) - sublinux: $(SUBTARGET))
    else
      LOCAL_LINUX_CONFIG_FILE := $(LINUX_DEFAULT_CONFIG_FILE)
    endif
  endif
else
  LOCAL_LINUX_CONFIG_FILE := $(LINUX_CONFIG_FILE)
endif

###############################################################################
###############################################################################

# Headers to be copied in $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)
LINUX_EXPORTED_HEADERS_OVER := \
	include/linux/media.h \
	include/linux/media-bus-format.h \
	include/linux/videodev2.h \
	include/linux/v4l2-common.h \
	include/linux/v4l2-controls.h \
	include/linux/v4l2-mediabus.h \
	include/linux/v4l2-subdev.h \
	include/linux/i2c-dev.h \
	include/linux/hid.h \
	include/linux/hidraw.h \
	include/linux/hiddev.h \
	include/linux/const.h \
	include/linux/ethtool.h \
	include/linux/net.h \
	include/linux/uinput.h \
	include/linux/input.h \
	include/linux/watchdog.h \
	include/linux/spi/spidev.h \
	include/linux/uhid.h \
	include/linux/ion.h \
	include/linux/sock_diag.h \
	include/linux/inet_diag.h \
	include/linux/iio/events.h \
	include/linux/iio/types.h \
	include/linux/cn_proc.h \
	include/linux/prctl.h \
	include/linux/input-event-codes.h \
	include/linux/mii.h

###############################################################################
###############################################################################

# Macro to copy a kernel image from boot directory to staging directory
# $1 image file to copy from arch/boot directory
linux-copy-image = \
	$(if $(call streq,$(PRIVATE_LINUX_IMAGE),$1), \
		if [ -f $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/$1 ]; then \
			cp --remove-destination -af -L $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/$1 $(TARGET_OUT_TRAIL_STAGING_PRE)/kernel$(PRIVATE_LINUX_SUFFIX).img; \
			cd $(TARGET_OUT_TRAIL_STAGING_PRE); \
		elif [ -f $(PRIVATE_LINUX_BUILD_DIR)/$1 ]; then \
			cp --remove-destination -af -L $(PRIVATE_LINUX_BUILD_DIR)/$1 $(TARGET_OUT_TRAIL_STAGING_PRE)/kernel$(PRIVATE_LINUX_SUFFIX).img; \
			cd $(TARGET_OUT_TRAIL_STAGING_PRE); \
		else \
		echo ERROR: bad linux-copy-image: $(PRIVATE_LINUX_IMAGE) == $(1) $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/$1; exit 1; \
		fi; \
	)

define linux-clean-modules-pvr
	rm -f $(TARGET_OUT_TRAIL_STAGING)/modules.squashfs
endef

define linux-copy-images
	$(Q) $(call linux-copy-image,uImage)
	$(Q) $(call linux-copy-image,Image)
	$(Q) $(call linux-copy-image,Image.gz)
	$(Q) $(call linux-copy-image,zImage)
	$(Q) $(call linux-copy-image,bzImage)
	$(Q) $(call linux-copy-image,vmlinux)
endef

###############################################################################
###############################################################################

ifneq ("$(TARGET_LINUX_LINK_CPIO_IMAGE)","0")

define linux-setup-cpio-config
	@ : > $(PRIVATE_LINUX_BUILD_DIR)/rootfs.cpio.gz
	$(call kconfig-enable-opt,CONFIG_BLK_DEV_INITRD,$(PRIVATE_LINUX_BUILD_DIR)/.config)
	$(call kconfig-set-opt,CONFIG_INITRAMFS_SOURCE,\"rootfs.cpio.gz\",$(PRIVATE_LINUX_BUILD_DIR)/.config)
	$(call kconfig-set-opt,CONFIG_INITRAMFS_ROOT_UID,0,$(PRIVATE_LINUX_BUILD_DIR)/.config)
	$(call kconfig-set-opt,CONFIG_INITRAMFS_ROOT_GID,0,$(PRIVATE_LINUX_BUILD_DIR)/.config)
	$(call kconfig-disable-opt,CONFIG_INITRAMFS_COMPRESSION_NONE,$(PRIVATE_LINUX_BUILD_DIR)/.config)
	$(call kconfig-enable-opt,CONFIG_INITRAMFS_COMPRESSION_GZIP,$(PRIVATE_LINUX_BUILD_DIR)/.config)
endef

else

define linux-setup-cpio-config
endef

endif

###############################################################################
###############################################################################

$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_MAKE_ARGS := $(LOCAL_LINUX_MAKE_ARGS)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_MERGE_CONFIG_ENV := $(LOCAL_LINUX_MERGE_CONFIG_ENV)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_CONFIG_TARGET := $(LOCAL_LINUX_CONFIG_TARGET)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_CONFIG_FILE := $(LOCAL_LINUX_CONFIG_FILE)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_IMAGE := $(LOCAL_LINUX_IMAGE)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_BUILD_DIR := $(LINUX_BUILD_DIR)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_SRCARCH := $(LOCAL_LINUX_SRCARCH)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_ARCH := $(LOCAL_LINUX_ARCH)
$(LOCAL_TARGETS) $(LINUX_BUILD_DIR)/.config: PRIVATE_LINUX_SUFFIX := $(LOCAL_LINUX_SUFFIX)

# Setup config in build dir
ifneq ("$(LOCAL_LINUX_CONFIG_FILE_IS_TARGET)","")

# if we have merge files, we use merge_config.sh
ifneq ("$(TARGET_LINUX_CONFIG_MERGE_FILES)","")
# Use merge-config tool
define linux-setup-merge-configs
	$(Q) $(PRIVATE_LINUX_MERGE_CONFIG_ENV) $(LINUX_DIR)/scripts/kconfig/merge_config.sh -m -O $(PRIVATE_LINUX_BUILD_DIR)/ $(PRIVATE_LINUX_BUILD_DIR)/.config $(wildcard $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).d/*.config $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).d/*.cfg) 
endef
endif

# Use linux target
define linux-setup-config
	@mkdir -p $(PRIVATE_LINUX_BUILD_DIR)
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) $(PRIVATE_LINUX_CONFIG_TARGET)
endef


# Copy it somewhere so after a dirclean it is not completely lost...
define linux-save-config
	$(Q) cp -af $(PRIVATE_LINUX_BUILD_DIR)/.config $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).config
	@echo "The linux config file has been saved in $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).config"
	@echo "If you do a 'linux-dirclean' you will need to do a 'linux-restore-config' to restore it"
	@echo "Otherwise the default target '$(PRIVATE_LINUX_CONFIG_TARGET)' will be used again."
endef

# Rule to create .config
$(LINUX_BUILD_DIR)/.config: $(wildcard $(LOCAL_LINUX_CONFIG_FILE)) $(wildcard $(TARGET_CONFIG_DIR)/$(LOCAL_LINUX_CONFIG_TARGET).d/*.config $(TARGET_CONFIG_DIR)/$(LOCAL_LINUX_CONFIG_TARGET).d/*.cfg)
	+$(linux-setup-config)
	+$(linux-setup-cpio-config)
	+$(linux-setup-merge-configs)

else # ifneq ("$(LOCAL_LINUX_CONFIG_FILE_IS_TARGET)","")

# Use a file

# if we have merge files, we use merge_config.sh
ifneq ("$(TARGET_LINUX_CONFIG_MERGE_FILES)","")
# Use merge-config tool
define linux-setup-merge-configs
	$(Q) $(PRIVATE_LINUX_MERGE_CONFIG_ENV) $(LINUX_DIR)/scripts/kconfig/merge_config.sh -m -O $(PRIVATE_LINUX_BUILD_DIR)/ $(PRIVATE_LINUX_BUILD_DIR)/.config $(wildcard $(PRIVATE_LINUX_CONFIG_FILE).d/*.config $(PRIVATE_LINUX_CONFIG_FILE).d/*.cfg)
endef
endif

define linux-setup-config
	@mkdir -p $(PRIVATE_LINUX_BUILD_DIR)
	@$(call __config-apply-sed,$(PRIVATE_MODULE),$(PRIVATE_LINUX_BUILD_DIR)/linux.config.tmp,$(PRIVATE_LINUX_CONFIG_FILE))
	$(Q) cp -af $(PRIVATE_LINUX_BUILD_DIR)/linux.config.tmp $(PRIVATE_LINUX_BUILD_DIR)/.config
endef

define linux-save-config
	$(Q) cp -af $(PRIVATE_LINUX_BUILD_DIR)/.config $(PRIVATE_LINUX_CONFIG_FILE)
endef

# Rule to create .config
$(LINUX_BUILD_DIR)/.config: $(LOCAL_LINUX_CONFIG_FILE) $(wildcard $(LOCAL_LINUX_CONFIG_FILE).d/*.config $(LOCAL_LINUX_CONFIG_FILE).d/*.cfg)
	+$(linux-setup-config)
	+$(linux-setup-cpio-config)
	+$(linux-setup-merge-configs)

endif # ifneq ("$(LOCAL_LINUX_CONFIG_FILE_IS_TARGET)","")

###############################################################################
###############################################################################

# Generate everything for the sdk (so we can build external kernel modules from it)
# Inspired from <linux>/scripts/package/builddeb, 'Build header package' section
LINUX_SDK_DIR := $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/src/$(LOCAL_MODULE)-sdk
define linux-gen-sdk
	$(Q) :> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles
	$(Q) :> $(PRIVATE_LINUX_BUILD_DIR)/sdkobjfiles
	$(Q) (cd $(PRIVATE_PATH); \
		find . -name Makefile -o -name Kconfig\* -o -name \*.pl \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles)
	$(Q) (cd $(PRIVATE_PATH); \
		find arch/$(PRIVATE_LINUX_SRCARCH)/include include scripts -type f \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles)
	$(Q) (cd $(PRIVATE_PATH); \
		find arch/$(PRIVATE_LINUX_SRCARCH)/kernel -type f -name '*.lds' \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles)
$(if $(call streq,$(PRIVATE_LINUX_ARCH),arm), \
	$(Q) (cd $(PRIVATE_PATH); \
		find arch/$(PRIVATE_LINUX_SRCARCH)/*/include -type f \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles) \
)
$(if $(call streq,$(PRIVATE_LINUX_ARCH),arm64), \
	$(Q) (cd $(PRIVATE_PATH); \
		find arch/$(PRIVATE_LINUX_SRCARCH)/include -type f \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles) \
)
	$(Q) (cd $(PRIVATE_LINUX_BUILD_DIR); \
		[ ! -d arch/$(PRIVATE_LINUX_SRCARCH)/include ] || \
		find arch/$(PRIVATE_LINUX_SRCARCH)/include include scripts .config Module.symvers -type f \
		>> $(PRIVATE_LINUX_BUILD_DIR)/sdkobjfiles)
	$(Q) mkdir -p $(LINUX_SDK_DIR)
	$(Q) $(TAR) -C $(PRIVATE_PATH) -cf - -T $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles | \
		$(TAR) -C $(LINUX_SDK_DIR) -xf -
	$(Q) $(TAR) -C $(PRIVATE_LINUX_BUILD_DIR) -cf - -T $(PRIVATE_LINUX_BUILD_DIR)/sdkobjfiles | \
		$(TAR) -C $(LINUX_SDK_DIR) -xf -
	$(Q) rm -f $(PRIVATE_LINUX_BUILD_DIR)/sdksrcfiles
	$(Q) rm -f $(PRIVATE_LINUX_BUILD_DIR)/sdkobjfiles
	$(Q) echo "$(PRIVATE_LINUX_ARCH)" > $(LINUX_SDK_DIR)/linuxarch
endef

###############################################################################
###############################################################################

# asac/XXX remove these if we dont see any issues in any builds
#$(LINUX_BUILD_DIR)/$(LOCAL_MODULE_FILENAME): PRIVATE_LINUX_SUFFIX := $(LOCAL_LINUX_SUFFIX)
#$(LINUX_BUILD_DIR)/$(LOCAL_MODULE_FILENAME): PRIVATE_LINUX_BUILD_DIR := $(LINUX_BUILD_DIR)

# Avoid compiling kernel at same time than header installation by adding a prerequisite
$(LINUX_BUILD_DIR)/$(LOCAL_MODULE_FILENAME): $(LINUX_BUILD_DIR)/.config $(LINUX_HEADERS_DONE_FILE)
	echo "Checking linux kernel config: $(PRIVATE_LINUX_CONFIG_FILE)"
	$(Q) yes "" 2>/dev/null | $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) oldconfig
	@echo "Building linux kernel"

# Run version gen script if needed
ifneq ("$(TARGET_VERSION_GEN_HELPER)","")
	$(TARGET_VERSION_GEN_HELPER)
endif

ifneq ("$(TARGET_LINUX_LINK_CPIO_IMAGE)","0")
	@ : > $(PRIVATE_LINUX_BUILD_DIR)/rootfs.cpio.gz
endif
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS)
	@echo "Installing linux kernel modules"
	#$(Q) rm -rf $(TARGET_OUT_STAGING)/lib/modules
	$(Q) if grep -q "CONFIG_MODULES=y" $(PRIVATE_LINUX_BUILD_DIR)/.config; then \
		$(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) modules_install ; \
		$(call linux-clean-modules-pvr) ; \
	else \
		echo "CONFIG_MODULES not set in kernel config, ignoring"; \
	fi
ifneq ("$(TARGET_LINUX_INSTALL_DEVICE_TREE)","0")
	@echo "Installing linux kernel dtbs"
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) dtbs_install
else ifneq ("$(TARGET_LINUX_DEVICE_TREE_NAMES)$(TARGET_LINUX_FIT_IMAGE_CONFIG_DTBS)$(TARGET_LINUX_FIT_IMAGE_CONFIG_OVLS)","")
	@echo "Building device tree binaries"
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) dtbs
	$(foreach __f,$(TARGET_LINUX_DEVICE_TREE_NAMES) $(TARGET_LINUX_FIT_IMAGE_CONFIG_DTBS) $(TARGET_LINUX_FIT_IMAGE_CONFIG_OVLS), \
		cp -af $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/dts/$(firstword $(subst @, ,$(__f))) \
			$(TARGET_OUT_TRAIL_STAGING_PRE)/$(endl) \
	)
endif
ifneq ("$(TARGET_LINUX_APPEND_RAW_DTB)","")
	@echo "Appending raw DTB to kernel object"
	$(Q) cat $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/dts/$(TARGET_LINUX_APPEND_RAW_DTB) >> $(PRIVATE_LINUX_BUILD_DIR)/vmlinux.bin
endif
ifneq ("$(TARGET_LINUX_APPEND_DTB)","")
	@echo "Building zImage"
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) zImage
	@echo "Appending DTB to zImage object"
	$(Q) cat $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/dts/$(TARGET_LINUX_APPEND_DTB) >> $(PRIVATE_LINUX_BUILD_DIR)/arch/$(PRIVATE_LINUX_SRCARCH)/boot/zImage
endif
	@echo "Installing linux kernel images"
ifneq ("$(TARGET_LINUX_GENERATE_UIMAGE)","0")
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) uImage
endif
ifeq ("$(LOCAL_LINUX_IMAGE)","uImage")
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) uImage
endif
	@mkdir -p $(TARGET_OUT_STAGING)/kernel
	$(call linux-copy-images)
	$(Q) cp -af $(PRIVATE_LINUX_BUILD_DIR)/vmlinux $(TARGET_OUT_STAGING)/kernel/vm$(PRIVATE_MODULE)
	$(call linux-gen-sdk)
	$(Q) cp -af $(PRIVATE_LINUX_BUILD_DIR)/.config $(PRIVATE_LINUX_BUILD_DIR)/linux.config
	$(Q) echo "$(PRIVATE_LINUX_ARCH)" > $(PRIVATE_LINUX_BUILD_DIR)/linuxarch
	@echo "Linux kernel built"
	@touch $@

###############################################################################
###############################################################################

# Linux headers
# Order-only dependency on config to avoid parallel execution of linux makefile
.PHONY: linux-headers
$(LOCAL_MODULE)-headers: $(call local-get-build-dir)/$(LOCAL_MODULE)-headers.done

$(call local-get-build-dir)/$(LOCAL_MODULE)-headers.done: PRIVATE_LINUX_MAKE_ARGS := $(LOCAL_LINUX_MAKE_ARGS)
$(call local-get-build-dir)/$(LOCAL_MODULE)-headers.done: PRIVATE_LINUX_BUILD_DIR := $(LINUX_BUILD_DIR)

$(call local-get-build-dir)/$(LOCAL_MODULE)-headers.done: | $(call local-get-build-dir)/.config
ifeq ("$(LOCAL_MODULE)", "linux")
ifneq ("$(LOCAL_LINUX_ARCH)","um")
	@mkdir -p $(PRIVATE_LINUX_BUILD_DIR)
	@mkdir -p $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/src/linux-headers
	@echo "Installing linux kernel headers"
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) headers_install
	@mkdir -p $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/include/linux
	@mkdir -p $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/include/linux/spi
	$(foreach header,$(LINUX_EXPORTED_HEADERS), \
		$(Q) install -m 0644 -p -D $(header) \
			$(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/include/linux/$(notdir $(header))$(endl) \
	)
	$(foreach header,$(LINUX_EXPORTED_HEADERS_OVER), \
		$(Q) if [ -f $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/src/linux-headers/$(header) ]; then \
			install -m 0644 -p -D $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/src/linux-headers/$(header) \
				$(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/$(header); \
		fi$(endl) \
	)
endif
endif
	@echo "Installing linux kernel headers: done"
	@touch $@

###############################################################################
###############################################################################

# Custom clean rule. LOCAL_MODULE_FILENAME already deleted by common rule
# make clean may fail, so ignore its error
.PHONY: $(LOCAL_MODULE)-clean
$(LOCAL_MODULE)-clean:
	$(Q) if [ -d $(PRIVATE_LINUX_BUILD_DIR) ]; then \
		$(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) --ignore-errors \
			clean || echo "Ignoring clean errors"; \
	fi
	$(Q) rm -rf $(TARGET_OUT_STAGING)/lib/modules
	$(Q) rm -f $(TARGET_OUT_STAGING)/kernel/Image
	$(Q) rm -f $(TARGET_OUT_STAGING)/kernel/zImage
	$(Q) rm -f $(TARGET_OUT_STAGING)/kernel/bzImage
	$(Q) rm -f $(TARGET_OUT_STAGING)/kernel/uImage
	$(Q) rm -f $(LINUX_HEADERS_DONE_FILE)
	$(Q) rm -rf $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/src/$(PRIVATE_MODULE)-headers
	$(Q) rm -rf $(LINUX_SDK_DIR)
	$(foreach header,$(LINUX_EXPORTED_HEADERS),\
		$(Q) rm -f $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/include/linux/$(notdir $(header))$(endl) \
	)
	$(foreach header,$(LINUX_EXPORTED_HEADERS_OVER),\
		$(Q) rm -f $(TARGET_OUT_STAGING)/$(TARGET_ROOT_DESTDIR)/$(header)$(endl) \
	)
ifneq ("$(TARGET_LINUX_LINK_CPIO_IMAGE)","0")
	$(Q) rm -f $(PRIVATE_LINUX_BUILD_DIR)/rootfs.cpio.gz
endif

###############################################################################
###############################################################################

# Kernel configuration
.PHONY: $(LOCAL_MODULE)-menuconfig
$(LOCAL_MODULE)-menuconfig: $(LINUX_BUILD_DIR)/.config
	@echo "Configuring linux kernel: $(PRIVATE_LINUX_CONFIG_FILE)"
	$(if $(call is-var-defined,custom.$(PRIVATE_MODULE).config.sedfiles), \
		$(error Sed files found. We cannot save in this case))
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) menuconfig
	$(Q) $(linux-save-config)

.PHONY: $(LOCAL_MODULE)-xconfig
$(LOCAL_MODULE)-xconfig: $(LINUX_BUILD_DIR)/.config
	@echo "Configuring linux kernel: $(PRIVATE_LINUX_CONFIG_FILE)"
	$(if $(call is-var-defined,custom.$(PRIVATE_MODULE).config.sedfiles), \
		$(error Sed files found. We cannot save in this case))
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) xconfig
	$(Q) $(linux-save-config)

.PHONY: $(LOCAL_MODULE)-config
$(LOCAL_MODULE)-config: $(LOCAL_MODULE)-xconfig

# Restore linux config file when using a config target
.PHONY: $(LOCAL_MODULE)-restore-config
$(LOCAL_MODULE)-restore-config:
ifneq ("$(LOCAL_LINUX_CONFIG_FILE_IS_TARGET)","")
	@mkdir -p $(PRIVATE_LINUX_BUILD_DIR)
	@echo "Restoring linux config: $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).config"
	$(Q) cp -af $(TARGET_CONFIG_DIR)/$(PRIVATE_LINUX_CONFIG_TARGET).config $(PRIVATE_LINUX_BUILD_DIR)/.config
endif

.PHONY: $(LOCAL_MODULE)-check-config
$(LOCAL_MODULE)-check-config: $(PRIVATE_LINUX_BUILD_DIR)/.config
	@echo "Checking linux config: $(PRIVATE_LINUX_CONFIG_FILE)"
	$(Q) yes "" 2>/dev/null | $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) oldconfig
	$(Q) diff -u $(PRIVATE_LINUX_CONFIG_FILE) $(PRIVATE_LINUX_BUILD_DIR)/.config || true

.PHONY: $(LOCAL_MODULE)-reset-config
$(LOCAL_MODULE)-reset-config:
	@echo "Reseting linux config: $(PRIVATE_LINUX_CONFIG_FILE)"
	$(Q) rm -f $(PRIVATE_LINUX_BUILD_DIR)/.config
	+$(Q) $(call linux-setup-config, $(PRIVATE_MODULE))

###############################################################################
###############################################################################

# Default rule to invoke kernel specific targets (like cscope, tags, help ...)
ifneq ("$(filter $(LOCAL_MODULE)-%,$(MAKECMDGOALS))","")
.PHONY: $(LOCAL_MODULE)-%
$(LOCAL_MODULE)-%: $(LINUX_BUILD_DIR)/.config
	@echo "Building linux kernel $* target with $(PRIVATE_LINUX_CONFIG_FILE)"
	$(Q) $(MAKE) $(PRIVATE_LINUX_MAKE_ARGS) $*
	$(Q) $(linux-save-config)
endif
