###############################################################################
## @file classes/UBOOT/rules.mk
## @author Ricardo Mendoza
## @date 2016/09/28
##
## Rules for UBOOT modules.
###############################################################################

_module_msg := $(if $(_mode_host),Host )U-Boot

include $(BUILD_SYSTEM)/classes/GENERIC/rules.mk

###############################################################################
###############################################################################

# Linux configuration file or target
local_module := $(LOCAL_MODULE)
ifdef SUBTARGET
       local_module = $(LOCAL_MODULE).$(SUBTARGET)
endif
# we use the first of submodule or local module uboot.config
UBOOT_CONFIG_FILE := $(firstword $(wildcard $(call module-get-config,$(local_module)) $(call module-get-config,$(LOCAL_MODULE))))

###############################################################################
###############################################################################

# if we have merge files, we use merge_config.sh
ifneq ("$(TARGET_UBOOT_CONFIG_MERGE_FILES)","")
# Use merge-config tool
define uboot-setup-merge-configs
	$(Q) $(UBOOT_MERGE_CONFIG_ENV) $(UBOOT_DIR)/scripts/kconfig/merge_config.sh -m -O $(UBOOT_BUILD_DIR)/ $(UBOOT_BUILD_DIR)/.config $(wildcard $(TARGET_CONFIG_DIR)/$(UBOOT_CONFIG_TARGET).d/*.config)
endef
endif

ifneq ("$(UBOOT_CONFIG_TARGET)","")

define uboot-setup-config
	@mkdir -p $(UBOOT_BUILD_DIR)
	$(Q) $(MAKE) $(UBOOT_MAKE_ARGS) $(UBOOT_CONFIG_TARGET)
endef

else # ifeq ("$(wildcard $(UBOOT_CONFIG_FILE))","")

# Use .config provided
define uboot-setup-config
	@mkdir -p $(UBOOT_BUILD_DIR)
	$(Q) cp -af $(UBOOT_CONFIG_FILE) $(UBOOT_BUILD_DIR)/.config
endef

endif

# Rule to create .config
$(UBOOT_BUILD_DIR)/.config: $(UBOOT_CONFIG_FILE) $(wildcard $(TARGET_CONFIG_DIR)/$(UBOOT_CONFIG_TARGET).d/*.config)
	+$(uboot-setup-config)
	+$(uboot-setup-merge-configs)


###############################################################################
###############################################################################

# Avoid compiling kernel at same time than header installation by adding a prerequisite
$(UBOOT_BUILD_DIR)/$(LOCAL_MODULE_FILENAME): $(UBOOT_BUILD_DIR)/.config 
	@echo "Checking U-Boot config: $(UBOOT_CONFIG_FILE)"
	$(Q) yes "" 2>/dev/null | $(MAKE) $(UBOOT_MAKE_ARGS) oldconfig
	@echo "Building U-Boot"

	$(Q) $(MAKE) $(UBOOT_MAKE_ARGS) $(UBOOT_MAKE_TARGETS)

	@mkdir -p $(TARGET_OUT_STAGING)/boot
	@if [ "$(UBOOT_MAKE_TARGETS)" != "tools-only" ]; then \
		cp -af $(UBOOT_BUILD_DIR)/u-boot.bin $(TARGET_OUT_STAGING)/boot; \
		echo "U-Boot built"; \
	else \
		echo "U-Boot Tools built"; \
	fi
	@touch $@

###############################################################################
###############################################################################

# Custom clean rule. LOCAL_MODULE_FILENAME already deleted by common rule
# make clean may fail, so ignore its error
.PHONY: uboot-clean
uboot-clean:
	$(Q) if [ -d $(UBOOT_BUILD_DIR) ]; then \
		$(MAKE) $(UBOOT_MAKE_ARGS) --ignore-errors \
			clean || echo "Ignoring clean errors"; \
	fi
