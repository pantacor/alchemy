#!/bin/bash

SCRIPT_PATH=$(cd $(dirname $0) && pwd -P)

CHECKERS=$1
ARGS=$2
FILES=$3
MODULE_DIR=$4

set -e

for CHECKER in ${CHECKERS}; do
	if [ "${CHECKER}" = "linux" ]; then
		${SCRIPT_PATH}/checkpatch.pl \
			--no-tree --no-summary --terse --show-types -f \
			${ARGS} \
			${FILES}
	elif [ "${CHECKER}" = "clang-format" ]; then
		${SCRIPT_PATH}/clang-format-check.sh "${FILES}" ${MODULE_DIR}
	elif [ "${CHECKER}" = "gst" ]; then
		${SCRIPT_PATH}/gst-indent-check.sh "${FILES}" ${MODULE_DIR}
	elif [ "${CHECKER}" = "clang" ]; then
		diff -u <(cat ${FILES}) <(clang-format-13 -style=file ${FILES})
		clang-tidy-13 ${FILES} -- -D_GNU_SOURCE=1 -std=gnu11 -Iinternal/init -Iinternal/init/utils -Iinternal/picohttpparser -Iinternal/libthttp -Iexternal/mbedtls/include -Iout/${TARGET}/staging/usr/include -ferror-limit=0
	else
		echo "Unknown 'c' checker '${CHECKER}'"
	fi
done
