#!/usr/bin/env python3

import json
from typing import Any, Dict, Final
import sbom_common as sbom
from datetime import datetime


DOCUMENT_ID: Final[str] = "SPDXRef-DOCUMENT"
OUTPUT_FILENAME: Final[str] = "sbom.spdx.json"


def SPDXDoc() -> Dict[str, Any]:
    return {
        "SPDX": DOCUMENT_ID,
        "creationInfo": {
            "created": str(datetime.now()),
            "creators": [
                "Organization: Pantacor",
                "Tool: Pantacor SBOM script",
            ],
        },
        "dataLicense": "",
        "name": "Pantavisor SBOM",
        "spdxVersion": "SPDX-2.3",
        "documentNamespace": "",
        "packages": [],
        "relationships": [],
    }


def create_relationship(pkg_id: str) -> Dict[str, str]:
    return {
        "spdxElementId": DOCUMENT_ID,
        "relatedSpdxElement": pkg_id,
        "relationshipType": "DESCRIBES",
    }


def create_package(name: str, rev: str, url: str, path: str) -> Dict[str, Any]:
    return {
        "SPDXID": "SPDXRef-{}-{}".format(name, rev),
        "downloadLocation": url,
        "name": name,
        "versionInfo": sbom.version_str(name, path, rev)
    }


def main() -> None:
    args = sbom.args_parser()
    doc = SPDXDoc()
    for name, rev, url, path in zip(
        args.mods, args.revs, args.urls, args.paths
    ):
        if name in sbom.SKIP_LIST:
            continue
        pkg = create_package(name, rev, url, path)
        rel = create_relationship(pkg["SPDXID"])
        doc["packages"].append(pkg)
        doc["relationships"].append(rel)

    out = args.out.joinpath(OUTPUT_FILENAME)
    out.write_text(json.dumps(doc, indent=4))


if __name__ == "__main__":
    main()
