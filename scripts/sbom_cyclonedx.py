#!/usr/bin/env python3

import uuid
import json
import sbom_common as sbom
from typing import Dict, Any, List
from urllib.parse import urlparse
from datetime import datetime
from pathlib import Path


class CyclonedxDoc:
    doc: Dict[str, Any]

    def __init__(self) -> None:
        self.doc = {
            "bomFormat": "CycloneDX",
            "specVersion": "1.4",
            "serialNumber": "urn:uuid:{}".format(uuid.uuid4()),
            "version": 1,
            "metadata": {
                "timestamp": datetime.isoformat(
                    datetime.now(datetime.now().astimezone().tzinfo)
                ),
                "tools": [
                    {"vendor": "Pantacor", "name": "Alchemy SBOM script"}
                ],
                "component": {},
            },
            "components": [],
        }

    def add_component(self, name: str, cmp: Dict[str, Any]) -> None:
        if name in sbom.COMPONENTS and sbom.COMPONENTS[name].get("main", False):
            self.doc["metadata"]["component"] = cmp
        else:
            self.doc["components"].append(cmp)

    def dump(self):
        return json.dumps(self.doc, indent=4)


def get_purl(
    type: str,
    name: str,
    version: str,
    qualifiers: str | None = None,
    namespace: str | None = None,
) -> str:
    if namespace:
        purl = "pkg:{}{}/{}@{}".format(type, namespace, name, version)
    else:
        purl = "pkg:{}{}@{}".format(type, name, version)

    if qualifiers:
        purl += "?" + qualifiers

    return purl


# All our current components come from a repositories,
# so this function use only that info as input
def info_from_uri(uri: str) -> Dict[str, str]:
    parsed = urlparse(uri)
    netloc = parsed.netloc.split(".")
    info = {}
    if netloc[0] == "gitlab" or netloc[0] == "github":
        info["type"] = netloc[0]
    elif netloc[1] == "launchpad":
        info["type"] = netloc[1]
    else:
        info["type"] = "unknown"

    p = Path(parsed.path)
    info["namespace"] = p.parent
    info["name"] = p.name

    info["qualifiers"] = parsed.query
    return info


def build_cpe(name:str, version: str, cpe_info: str) -> str:
    cpe = "cpe:2.3"

    type = sbom.COMPONENTS[name].get("type", "application")
    vendor = sbom.COMPONENTS[name].get("vendor", name)

    if type == "operanting-system":
        cpe += ":o"
    elif type == "device":
        cpe += ":h"
    elif type == "application":
        cpe += ":a"
    else:
        cpe += ":*"

    if cpe_info:
        cpe += ":" + cpe_info
    else:
        cpe += ":" + vendor
        cpe += ":" + name

    cpe += ":" + version
    return cpe + ":*:*:*:*:*:*:*"

def get_cpe(name: str, version: str) -> str:

    if name not in sbom.COMPONENTS:
        return "not-existent"

    cpe_info = sbom.COMPONENTS[name].get("cpe", None)
    if cpe_info == "not-existent":
        return "not-existent"

    return build_cpe(name, version, cpe_info)

def get_cyclonedx_hashes(name: str, bin_path: Path) -> List[Dict[str, str]]:
    hashes = sbom.get_hashes(name, bin_path)
    if not hashes["hashes"]:
        return [{}]

    return [{"alg": hashes["type"], "content": h} for h in hashes["hashes"]]


def new_component(
    name: str, rev: str, uri: str, path: str, out: Path
) -> Dict[str, Any]:
    version = sbom.version_str(name, path, rev)
    info = info_from_uri(uri)
    purl = get_purl(
        info["type"],
        info["name"],
        version,
        info["qualifiers"],
        info["namespace"],
    )

    if name not in sbom.COMPONENTS:
        type = "unknown"
        json_name = name
        version_raw = ""
    else:
        type = sbom.COMPONENTS[name].get("type", "unknown")
        json_name = sbom.COMPONENTS[name].get("alias", name)
        if "ver" not in sbom.COMPONENTS[name]:
            version_raw = ""
        else:
            version_raw = sbom.run_cmd(path, sbom.COMPONENTS[name]["ver"])

    component = {
        "bom-ref": purl,
        "type": type,
        "name": json_name,
        "version": version,
        "cpe": get_cpe(name, version_raw),
        "hashes": get_cyclonedx_hashes(name, out),
        "purl": purl,
        "uri": uri,
        "licenses": sbom.scan_lic(path),
    }

    return component


def main():
    args = sbom.args_parser()
    doc = CyclonedxDoc()

    for name, rev, uri, path in zip(
        args.mods, args.revs, args.urls, args.paths
    ):
        if name in sbom.SKIP_LIST:
            continue
        doc.add_component(name, new_component(name, rev, uri, path, args.out))

    out = args.out.joinpath("sbom.cyclonedx.json")
    out.write_text(doc.dump())


if __name__ == "__main__":
    main()
