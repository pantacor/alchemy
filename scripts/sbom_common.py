import subprocess
import argparse
import re
import hashlib
import tempfile
from typing import Final, List, Tuple, Dict, Any
from pathlib import Path

COMPONENTS = {
    "argp-standalone": {
        "type": "library",
        "ver": rf"cat configure.ac | grep standalone-[0-9] | sed 's/[^0-9.]*//g' | tr -d '\n'",
        "binaries": [{"exp": "libargp.a", "path": "../staging/usr/lib/"}],
        "cpe": "not-existent",
    },
    "busybox": {
        "type": "application",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL'| sed 's/[^0-9]*//g'| tr '\n' '.' | head -c -1",
        "binaries": [{"exp": "busybox", "path": "./"}],
    },
    "cryptsetup": {
        "type": "application",
        "ver": rf"cat configure.ac | grep AC_INIT | sed 's/[^0-9.]*//g'| tr -d '\n'",
        "binaries": [{"exp": "*cryptsetup*", "path": "./"}],
        "cpe": "cryptsetup_project:cryptsetup",
    },
    "dropbear-pv": {
        "type": "application",
        "ver": rf"head -1 CHANGES | cut -d ' ' -f 1 | tr -d '\n'",
        "binaries": [{"exp": "dropbear*", "path": "./"}],
        "cpe": "dropbear_ssh_project:dropbear_ssh",
    },
    "e2fsprogs": {
        "type": "application",
        "ver": rf"cat version.h | grep E2FSPROGS_VERSION | cut -d ' ' -f3 | sed 's/[^0-9.]*//g' | tr -d '\n'",
        "binaries": [
            {"exp": "chattr", "path": "./"},
            {"exp": "lsattr", "path": "./"},
            {"exp": "mke2fs", "path": "./"},
            {"exp": "resize2fs", "path": "./"},
        ],
        "cpe": "e2fsprogs_project:e2fsprogs",
    },
    "pv_lxc": {
        "type": "library",
        "binaries": [{"exp": "pv_lxc.so", "path": "lib"}],
        "cpe": "not-existent",
    },
    "json-c": {
        "type": "library",
        "ver": rf"cat CMakeLists.txt | grep project | grep VERSION | sed 's/[^0-9.]*//g'| tr -d '\n'",
        "binaries": [{"exp": "libjson-c*", "path": "./"}],
    },
    "keyutils": {
        "type": "application",
        "ver": rf"cat keyutils.spec | grep -E 'define vermajor|define verminor' | sed 's/[^0-9.]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "keyutils_project:keyutils",
    },
    "libaio": {
        "type": "library",
        "ver": rf"cat libaio.spec | grep Version | cut -d ' ' -f2 | tr -d '\n'",
        "binaries": [{"exp": "libaio.so*", "path": "./"}],
        "cpe": "libaio_project:libaio",
    },
    "libcap": {
        "type": "library",
        "ver": rf"cat Make.Rules | grep -E 'VERSION|MINOR' | tr '\n' '.' | sed 's/[^0-9.]*//g' | head -c -1",
        "binaries": [{"exp": "libcap.so*", "path": "./"}],
        "cpe": "libcap_project:libcap",
    },
    "libkcapi": {
        "type": "library",
        "ver": rf"cat configure.ac| grep m4_define | sed 's/^.*,//g' | sed 's/[^0-9]*//g'| tr '\n' '.' | head -c -2",
        "binaries": [{"exp": ".*kcapi*", "path": "./"}],
        "cpe": "not-existent",
    },
    "libseccomp": {
        "type": "library",
        "ver": rf"cat CHANGELOG | grep Version | head -1| cut -d '-' -f1 | sed 's/[^0-9.]*//g' | tr -d '\n'",
        "binaries": [{"exp": "libseccomp.so*", "path": "./"}],
        "cpe": "libseccomp_project:libseccomp",
    },
    "lvm2": {
        "type": "application",
        "ver": rf"cat VERSION | cut -d ' ' -f1 | tr -d '\n'",
        "binaries": [{"exp": "lvm", "path": "./"}],
        "cpe": "redhat:lvm2",
    },
    "lxc": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\[lxc_version_.*\], [0-9]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "binaries": [
            {"exp": "lxc*", "path": "usr/bin"},
            {"exp": "lxc*", "path": "usr/libexec"},
            {"exp": "liblxc.so*", "path": "./"},
        ],
        "cpe": "linuxcontainers:lxc",
    },
    "mbedtls": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep '= mbed' | head -1 | tr ' ' '\n' | grep -E '[0-9]+\.[0-9]+\.[0-9]+' | tr -d '\n'",
        "binaries": [{"exp": "libmbedtls.a", "path": "../staging/usr/lib/"}],
        "cpe": "not-existent",
    },
    "mtools": {
        "type": "application",
        "ver": rf"cat version.texi| grep VERSION|cut -d ' ' -f3 | tr -d '\n'",
        "cpe": "not-existent",
    },
    "openssl": {
        "type": "library",
        "ver": rf"cat VERSION.dat | grep -E 'MAJOR|MINOR|PATCH'| cut -d '=' -f 2 | tr '\n' '.' | head -c -1",
        "binaries": [{"exp": "libcrypto.so*", "path": "./"}],
    },
    "qemu": {
        "type": "application",
        "ver": rf"cat VERSION | tr -d '\n'",
    },
    "rng-tools": {
        "type": "application",
        "ver": rf"cat configure.ac | grep AC_INIT | sed 's/[^0-9.]*//g'| head -c -2",
        "binaries": [{"exp": "rngd", "path": "./"}],
        "cpe": "rng-tools_project:rng-tools",
    },
    "zlib": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep 'Changes in' | head -1 | cut -d ' ' -f3 | tr -d '\n'",
        "binaries": [{"exp": "libz.so*", "path": "./"}],
    },
    "kmod": {
        "type": "application",
        "ver": rf"head -10 configure.ac | grep -E '\[[0-9]+\]'| sed 's/[^0-9]*//g' | tr -d '\n'",
        "binaries": [
            {"exp": "libkmod.so*", "path": "./"},
            {"exp": "kmod", "path": "./"},
        ],
    },
    "alchemy": {
        "type": "application",
        "ver": rf"cat main.mk | grep -E '^ALCHEMY_VERSION_.*' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "not-existent",
    },
    "glib": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\[glib_.*_version\], \[[0-9]+\]'| sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "gnome:glib",
    },
    "gpgme": {
        "type": "library",
        "ver": rf"cat configure.ac | grep m4_define| grep -oE '\[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "gnupg:gpgme",
    },
    "libassuan": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\], \[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "not-existent",
    },
    "libffi": {
        "type": "library",
        "ver": rf"cat configure.ac | grep AC_INIT | cut -d ',' -f2 | sed 's/[^0-9.]*//g' | tr -d '\n'",
        "cpe": "libffi_project:libffi",
    },
    "libgpg-error": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE 'minor\], \[[0-9]+\]|major\], \[[0-9\]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "gnupg" "libgpg-error",
    },
    "lttng-modules": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep -E '[0-9]{4}-[0-9]{2}-[0-9]{2}' | head -1 | grep -oE '[^ ]+$'|tr -d '\n'",
        "cpe": "not-existent",
    },
    "ostree": {
        "type": "library",
        "ver": rf"cat configure.ac | grep m4_define| grep -oE '\[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
        "cpe": "ostree_project:ostree",
    },
    "init": {
        "main": True,
        "alias": "Pantavisor",
        "type": "operating-system",
        "ver": rf"echo -n $(git describe --tags)-$(date +'%y%m%d')",
        "binaries": [{"exp": "init", "path": "./"}],
        "cpe": "not-existent",
    },
    "popt": {
        "type": "library",
        "ver": rf"cat configure.ac | grep AC_INIT | grep -oE '\[[0-9].*\],' | sed 's/[^0-9.]*//g' | tr -d '\n'",
        "binaries": [{"exp": "libpopt.so*", "path": "./"}],
        "cpe": "popt_project:popt",
    },
    "libpvlogger": {
        "type": "library",
        "binaries": [{"exp": "libpvlogger.a", "path": "../staging/usr/lib/"}],
        "cpe": "not-existent",
    },
    "libthttp": {
        "type": "library",
        "binaries": [{"exp": "libthttp.a", "path": "../staging/usr/lib/"}],
        "cpe": "not-existent",
    },
    "cypress_backports": {
        "type": "library",
        "sub-type": "module",
        "binaries": [{"exp": "*.ko", "path": "updates"}],
        "ver": rf"cat versions | grep BACKPORTS_VERSION | cut -d '=' -f2 | tr -d '\"' | tr -d '\n'",
        "cpe": "not-existent",
    },
    "picohttpparser": {
        "type": "library",
        "binaries": [{"exp": "picohttpparser.a", "path": "../staging/usr/lib"}],
        "cpe": "not-existent",
    },
    "uboot": {
        "type": "library",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL|^EXTRAVERSION'|sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -3",
        "cpe": "not-existent",
    },
    "linux": {
        "type": "library",
        "sub-type": "kernel",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL|^EXTRAVERSION'|sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -2",
        "cpe": "linux:linux_kernel",
    },
    "pv-smm-m2/symana-elp-signing-tools": {
        "type": "application",
        "ver": rf"cat version/tool_version.json | jq -r .version | tr -d '\n'",
        "cpe": "not-existent",
    },
    "wireguard-linux-compat": {
        "type": "library",
        "sub-type": "module",
        "binaries": [{"exp": "wireguard.ko", "path": "./"}],
        "ver": rf"cat src/version.h | grep '#define' | cut -d '\"' -f 2 | tr -d '\n'",
    },
    "xz": {
        "type": "library",
        "ver": rf"bash build-aux/version.sh",
        "cpe": "xz_project:xz",
    },
    "vocalfusion": {
        "type": "library",
        "sub-type": "module",
        "ver": rf"cat CHANGELOG.md | grep '##' | head -1 | grep -oE '[0-9].+' | tr -d '\n'",
        "binaries": [{"exp": "*.ko", "path": "extra"}],
        "cpe": "not-existent",
    },
    "apparmor": {
        "type": "application",
        "ver": rf"cat ../../common/Version | tr -d '\n'",
        "binaries": [{"exp": "libapparmor.so*", "path": "./"}],
    },
    "apparmor-parser": {
        "type": "library",
        "ver": rf"cat ../common/Version | tr -d '\n'",
        "binaries": [{"exp": "apparmor_parser", "path": "./"}],
        "cpe": "apparmor:apparmor",
    },
}

# this modules should be skiped because:
# init-dm: is the same than init
# init-crypt : is the same than init
# libc: is alchemy... (why??)
SKIP_LIST: Final[str] = ["init-dm", "init-crypt", "libc"]
LICENSEE_CMD: Final[str] = "licensee detect ./"
LICENSE_MIN_PROB: Final[float] = 78.0


def run_cmd(workdir: str, cmd: str) -> str:
    if not Path(workdir).exists():
        raise RuntimeError("path {} doesn't exists".format(workdir))

    command_complete = "cd {}; {}".format(workdir, cmd)
    result = subprocess.Popen(
        command_complete,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ).communicate()

    if result[1]:
        raise RuntimeError(
            "Cannot run command '{}': {}".format(command_complete, result[1])
        )

    return result[0].decode("utf8")


def version_str(name: str, path: str, rev: str) -> str:
    if name not in COMPONENTS or "ver" not in COMPONENTS[name]:
        return rev

    return run_cmd(path, COMPONENTS[name]["ver"]) + "+" + rev


def level(key: str) -> int:
    lvl = 0
    for k in key:
        if k != " ":
            break
        lvl += 1
    return lvl


def licensee_as_dict(
    data_list: List[str], start_at=0, lvl=0
) -> Tuple[Dict[str, Any], int]:
    data = {}

    lvl = level(data_list[start_at])
    i = start_at

    while i < len(data_list):
        kv = data_list[i].split(":")
        if len(kv) < 2:
            i += 1
            continue

        if level(kv[0]) < lvl:
            return data, i - 1

        kv[0] = kv[0].strip()
        if "," in kv[1] or kv[0] == "Matched files":
            data[kv[0]] = kv[1].split(",")
        elif not kv[1]:
            data[kv[0]], i = licensee_as_dict(data_list, i + 1, 0)
        else:
            data[kv[0]] = kv[1]

        i += 1

    return data, i - 1


def extract_squashfs(file: str, folder: str) -> None:
    run_cmd("./", "unsquashfs -f -d {} {}".format(folder, file))


def hash_file(path: Path) -> str | None:
    return hashlib.sha256(path.read_bytes()).hexdigest()


def hash_component(name: str, path: Path) -> List[str]:
    patterns = COMPONENTS[name].get("binaries", None)
    if not patterns:
        return []

    hashes = []
    for p in patterns:
        dir = path.joinpath(p["path"]).as_posix()
        result = run_cmd("./", 'find {} -iname "{}"'.format(dir, p["exp"]))

        for r in result.split():
            file = Path(r)
            if file.is_symlink() or not file.exists() or file.is_dir():
                continue
            hashes.append(hash_file(file))
    return hashes


def hash_kernel(out: Path) -> List[str]:
    path = out.joinpath("trail/final/trails/0/bsp/")
    kernel = path.joinpath("kernel.img")
    if kernel.exists():
        return [hash_file(kernel)]

    fit = path.joinpath("pantavisor.fit")
    if not fit.exists():
        return []

    result = run_cmd(
        "./",
        "dumpimage -p2 -l {} | grep -E 'Image|Hash' | grep -vE 'Type|FIT'".format(
            fit
        ),
    )

    result = [line for line in result.split("\n")]
    try:
        for i in range(0, len(result)):
            if "kernel" not in result[i]:
                continue
            return [result[i + 2].split(":")[1].strip()]
    except:
        return []


def hash_module(name: str, out: Path) -> List[str]:
    modules = out.joinpath("trail/final/trails/0/bsp/modules.squashfs")
    with tempfile.TemporaryDirectory() as fld:
        extract_squashfs(modules, fld)
        return hash_component(name, Path(fld))


def get_hashes(name: str, out: Path) -> Dict[str, List[str]]:
    hashes = {"type": "sha256", "hashes": []}

    if name not in COMPONENTS:
        return hashes
    stype = COMPONENTS[name].get("sub-type", "component")
    if stype == "component":
        hashes["hashes"] = hash_component(name, out.joinpath("final"))
    elif stype == "kernel":
        hashes["hashes"] = hash_kernel(out)
    elif stype == "module":
        hashes["hashes"] = hash_module(name, out)
    return hashes


def scan_lic(path: str) -> List[Dict[str, Dict[str, str]]]:
    result = run_cmd(path, LICENSEE_CMD).replace("similarity", "")
    result = re.sub(": +", ":", result)
    result = re.sub(", +", ",", result)

    data, _ = licensee_as_dict(result.split("\n"))
    lic = []
    try:
        matched = data["Matched files"]
    except:
        return [{}]

    for m in matched:
        lic_data = data[m]

        if lic_data["License"] != "NOASSERTION":
            found = {"license": {"id": lic_data["License"]}}
            if found not in lic:
                lic.append(found)
            continue

        cur_prob = 0.0
        cur_name = ""
        for k, v in lic_data["Closest non-matching licenses"].items():
            prob = float(v[:-1])
            if cur_prob > prob:
                continue
            cur_prob = prob
            cur_name = k

        if cur_prob > LICENSE_MIN_PROB:
            found = {"license": {"id": cur_name}}
            if found not in lic:
                lic.append(found)

    return lic


def args_parser() -> Any:
    parser = argparse.ArgumentParser()
    parser.add_argument("--mods", nargs="+")
    parser.add_argument("--revs", nargs="+")
    parser.add_argument("--urls", nargs="+")
    parser.add_argument("--paths", nargs="+")
    parser.add_argument("--out", type=Path)
    return parser.parse_args()
