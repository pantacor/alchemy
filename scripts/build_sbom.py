#!/usr/bin/env python3

import uuid
import re
import subprocess
import argparse
import json
from typing import Dict, Any, Final, List, Tuple
from urllib.parse import urlparse
from datetime import datetime
from pathlib import Path


COMPONENTS = {
    "argp-standalone": {
        "type": "library",
        "ver": rf"cat configure.ac | grep standalone-[0-9] | sed 's/[^0-9.]*//g' | tr -d '\n'",
    },
    "busybox": {
        "type": "application",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL'| sed 's/[^0-9]*//g'| tr '\n' '.' | head -c -1",
    },
    "cryptsetup": {
        "type": "application",
        "ver": rf"cat configure.ac | grep AC_INIT | sed 's/[^0-9.]*//g'| tr -d '\n'",
    },
    "dropbear-pv": {
        "type": "application",
        "ver": rf"head -1 CHANGES | cut -d ' ' -f 1 | tr -d '\n'",
    },
    "e2fsprogs": {
        "type": "application",
        "ver": rf"cat version.h | grep E2FSPROGS_VERSION | cut -d ' ' -f3 | sed 's/[^0-9.]*//g' | tr -d '\n'",
    },
    "json-c": {
        "type": "library",
        "ver": rf"cat CMakeLists.txt | grep project | grep VERSION | sed 's/[^0-9.]*//g'| tr -d '\n'",
    },
    "keyutils": {
        "type": "application",
        "ver": rf"cat keyutils.spec | grep -E 'define vermajor|define verminor' | sed 's/[^0-9.]*//g' | tr '\n' '.' | head -c -1",
    },
    "libaio": {
        "type": "library",
        "ver": rf"cat libaio.spec | grep Version | cut -d ' ' -f2 | tr -d '\n'",
    },
    "libcap": {
        "type": "library",
        "ver": rf"cat Make.Rules | grep -E 'VERSION|MINOR' | tr '\n' '.' | sed 's/[^0-9.]*//g' | head -c -1",
    },
    "libkcapi": {
        "type": "library",
        "ver": rf"cat configure.ac| grep m4_define | sed 's/^.*,//g' | sed 's/[^0-9]*//g'| tr '\n' '.' | head -c -2",
    },
    "libseccomp": {
        "type": "library",
        "ver": rf"cat CHANGELOG | grep Version | head -1| cut -d '-' -f1 | sed 's/[^0-9.]*//g' | tr -d '\n'",
    },
    "lvm2": {
        "type": "application",
        "ver": rf"cat VERSION | cut -d ' ' -f1 | tr -d '\n'",
    },
    "lxc": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\[lxc_version_.*\], [0-9]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "mbedtls": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep '= mbed' | head -1 | tr ' ' '\n' | grep -E '[0-9]+\.[0-9]+\.[0-9]+' | tr -d '\n'",
    },
    "mtools": {
        "type": "application",
        "ver": rf"cat version.texi| grep VERSION|cut -d ' ' -f3 | tr -d '\n'",
    },
    "openssl": {
        "type": "library",
        "ver": rf"cat VERSION.dat | grep -E 'MAJOR|MINOR|PATCH'| cut -d '=' -f 2 | tr '\n' '.' | head -c -1",
    },
    "qemu": {
        "type": "application",
        "ver": rf"cat VERSION | tr -d '\n'",
    },
    "rng-tools": {
        "type": "application",
        "ver": rf"cat configure.ac | grep AC_INIT | sed 's/[^0-9.]*//g'| head -c -2",
    },
    "zlib": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep 'Changes in' | head -1 | cut -d ' ' -f3 | tr -d '\n'",
    },
    "kmod":{
        "type": "application",
        "ver": rf"head -10 configure.ac | grep -E '\[[0-9]+\]'| sed 's/[^0-9]*//g' | tr -d '\n'",
    },
    "alchemy": {
        "type": "application",
        "ver": rf"cat main.mk | grep -E '^ALCHEMY_VERSION_.*' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "glib": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\[glib_.*_version\], \[[0-9]+\]'| sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "gpgme": {
        "type": "library",
        "ver": rf"cat configure.ac | grep m4_define| grep -oE '\[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "libassuan": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE '\], \[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "libffi": {
        "type": "library",
        "ver": rf"cat configure.ac | grep AC_INIT | cut -d ',' -f2 | sed 's/[^0-9.]*//g' | tr -d '\n'",
    },
    "libgpg-error": {
        "type": "library",
        "ver": rf"cat configure.ac | grep -oE 'minor\], \[[0-9]+\]|major\], \[[0-9\]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "lttng-modules": {
        "type": "library",
        "ver": rf"cat ChangeLog | grep -E '[0-9]{4}-[0-9]{2}-[0-9]{2}' | head -1 | grep -oE '[^ ]+$'|tr -d '\n'",
    },
    "ostree": {
        "type": "library",
        "ver": rf"cat configure.ac | grep m4_define| grep -oE '\[[0-9]+\]' | sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -1",
    },
    "init": {
            "main": True,
        "type": "operanting-system",
        "ver": rf"echo -n $(git describe --tags)-$(date +'%y%m%d')",
    },
    "popt": {
        "type": "library",
        "ver": rf"cat configure.ac | grep AC_INIT | grep -oE '\[[0-9].*\],' | sed 's/[^0-9.]*//g' | tr -d '\n'",
    },
    "cypress-backports": {
        "type": "library",
        "ver": rf"cat versions | grep BACKPORTS_VERSION | cut -d '=' -f2 | tr -d '\"' | tr -d '\n'",
    },
    "uboot": {
        "type": "library",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL|^EXTRAVERSION'|sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -3",
    },
    "linux": {
        "type": "library",
        "ver": rf"cat Makefile | grep -E '^VERSION|^PATCHLEVEL|^SUBLEVEL|^EXTRAVERSION'|sed 's/[^0-9]*//g' | tr '\n' '.' | head -c -2",
    },
    "pv-smm-m2/symana-elp-signing-tools": {
        "type": "application",
        "ver": rf"cat version/tool_version.json | jq -r .version | tr -d '\n'",
    },
    "wireguard-linux-compat": {
        "type": "library",
        "ver": rf"cat src/version.h | grep '#define' | cut -d '\"' -f 2 | tr -d '\n'",
    },
    "xz": {
        "type": "library",
        "ver": rf"bash build-aux/version.sh",
    },
    "vocalfusion": {
        "type": "library",
        "ver": rf"cat CHANGELOG.md | grep '##' | head -1 | grep -oE '[0-9].+' | tr -d '\n'",
    },
    "apparmor": {
        "type": "application",
        "ver": rf"cat ../../common/Version | tr -d '\n'",
    },
    "apparmor-parser": {
        "type": "library",
        "ver": rf"cat ../common/Version | tr -d '\n'",
    },
}

# this modules should be skiped because:
# init-dm: is the same than init
# init-crypt : is the same than init
# libc: is alchemy... (why??)
SKIP_LIST: Final[str] = ["init-dm", "init-crypt", "libc"]
LICENSEE_CMD: Final[str] = "licensee detect ./"
LICENSE_MIN_PROB: Final[float] = 78.0


class Doc:
    doc: Dict[str, Any]

    def __init__(self) -> None:
        self.doc = {
            "bomFormat": "CycloneDX",
            "specVersion": "1.4",
            "serialNumber": "urn:uuid:{}".format(uuid.uuid4()),
            "version": 1,
            "metadata": {
                "timestamp": datetime.isoformat(
                    datetime.now(datetime.now().astimezone().tzinfo)
                ),
                "tools": [
                    {"vendor": "Pantacor", "name": "Alchemy SBOM script"}
                ],
                "component": {},
            },
            "components": [],
        }

    def add_component(self, name: str, cmp: Dict[str, Any]) -> None:

        if name in COMPONENTS and COMPONENTS[name].get("main", False):
            self.doc["metadata"]["component"] = cmp
        else:
            self.doc["components"].append(cmp)

    def dump(self):
        return json.dumps(self.doc, indent=4)


def get_purl(
    type: str,
    name: str,
    version: str,
    qualifiers: str | None = None,
    namespace: str | None = None,
) -> str:
    if namespace:
        purl = "pkg:{}{}/{}@{}".format(type, namespace, name, version)
    else:
        purl = "pkg:{}{}@{}".format(type, name, version)

    if qualifiers:
        purl += "?" + qualifiers

    return purl


def run_cmd(workdir: str, cmd: str) -> str:
    if not Path(workdir).exists():
        raise RuntimeError("path {} doesn't exists".format(workdir))

    command_complete = "cd {}; {}".format(workdir, cmd)
    result = subprocess.Popen(
        command_complete,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ).communicate()

    if result[1]:
        raise RuntimeError(
            "Cannot run command '{}': {}".format(command_complete, result[1])
        )

    return result[0].decode("utf8")


def version_str(name: str, path: str, rev: str) -> str:
    if name not in COMPONENTS:
        return rev

    return run_cmd(path, COMPONENTS[name].get("ver", "")) + "+" + rev


# All our current components come from a repositories,
# so this function use only that info as input
def info_from_uri(uri: str) -> Dict[str, str]:
    parsed = urlparse(uri)
    netloc = parsed.netloc.split(".")
    info = {}
    if netloc[0] == "gitlab" or netloc[0] == "github":
        info["type"] = netloc[0]
    elif netloc[1] == "launchpad":
        info["type"] = netloc[1]
    else:
        info["type"] = "unknown"

    p = Path(parsed.path)
    info["namespace"] = p.parent
    info["name"] = p.name

    info["qualifiers"] = parsed.query
    return info


def get_cpe(name: str, version: str) -> str:
    cpe = "cpe:2.3"

    if name not in COMPONENTS:
        type = "application"
        vendor = name
    else:
        type = COMPONENTS[name].get("type", "application")
        vendor = COMPONENTS[name].get("vendor", name)

    if type == "operanting-system":
        cpe += ":o"
    elif type == "device":
        cpe += ":h"
    elif type == "application":
        cpe += ":a"
    else:
        cpe += ":*"

    cpe += ":" + vendor
    cpe += ":" + name
    cpe += ":" + version
    return cpe + ":*:*:*:*:*:*:*"


def level(key: str) -> int:
    lvl = 0
    for k in key:
        if k != " ":
            break
        lvl += 1
    return lvl


def licensee_as_dict(
    data_list: List[str], start_at=0, lvl=0
) -> Tuple[Dict[str, Any], int]:
    data = {}

    lvl = level(data_list[start_at])
    i = start_at

    while i < len(data_list):
        kv = data_list[i].split(":")
        if len(kv) < 2:
            i += 1
            continue

        if level(kv[0]) < lvl:
            return data, i - 1

        kv[0] = kv[0].strip()
        if "," in kv[1] or kv[0] == "Matched files":
            data[kv[0]] = kv[1].split(",")
        elif not kv[1]:
            data[kv[0]], i = licensee_as_dict(data_list, i + 1, 0)
        else:
            data[kv[0]] = kv[1]

        i += 1

    return data, i - 1


def scan_lic(path: str) -> List[Dict[str, Dict[str, str]]]:
    result = run_cmd(path, LICENSEE_CMD).replace("similarity", "")
    result = re.sub(": +", ":", result)
    result = re.sub(", +", ",", result)

    data, _ = licensee_as_dict(result.split("\n"))
    lic = []
    try:
        matched = data["Matched files"]
    except:
        return [{}]

    for m in matched:
        lic_data = data[m]

        if lic_data["License"] != "NOASSERTION":
            found = {"license": {"id": lic_data["License"]}}
            if found not in lic:
               lic.append(found)
            continue

        cur_prob = 0.0
        cur_name = ""
        for k, v in lic_data["Closest non-matching licenses"].items():
            prob = float(v[:-1])
            if cur_prob > prob:
                continue
            cur_prob = prob
            cur_name = k

        if cur_prob > LICENSE_MIN_PROB:
            found = {"license": {"id": cur_name}}
            if found not in lic:
                lic.append(found)

    return lic


def new_component(name: str, rev: str, uri: str, path: str) -> Dict[str, Any]:
    version = version_str(name, path, rev)
    info = info_from_uri(uri)
    purl = get_purl(
        info["type"],
        info["name"],
        version,
        info["qualifiers"],
        info["namespace"],
    )

    version = version_str(name, path, rev)

    if name not in COMPONENTS:
        type = "unknown"
    else:
        type = COMPONENTS[name].get("type", "unknown")

    component = {
        "bom-ref": purl,
        "type": type,
        "name": name,
        "version": version,
        "cpe": get_cpe(name, version),
        "purl": purl,
        "uri": uri,
        "licenses": scan_lic(path),
    }

    return component


def args_parser() -> Any:
    parser = argparse.ArgumentParser()
    parser.add_argument("--mods", nargs="+")
    parser.add_argument("--revs", nargs="+")
    parser.add_argument("--urls", nargs="+")
    parser.add_argument("--paths", nargs="+")
    parser.add_argument("--out", type=Path)
    return parser.parse_args()


def main():
    args = args_parser()
    doc = Doc()

    for name, rev, uri, path in zip(
        args.mods, args.revs, args.urls, args.paths
    ):
        if name in SKIP_LIST:
            continue
        doc.add_component(name, new_component(name, rev, uri, path))

    out = args.out.joinpath("sbom.json")
    out.write_text(doc.dump())


if __name__ == "__main__":
    main()
