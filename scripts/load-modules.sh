#!/bin/sh

TARGET_EARLY_MODULES_CONFIG=${1}
TARGET_OUT_STAGING=${2}
TARGET_OUT_FINAL=${3}
TARGET_FIRMWARE_DIR=${4}
TARGET_VENDOR_DIR=${5}
BUILD_SYSTEM=${6}
verbose=${7}

version=$(ls ${TARGET_OUT_STAGING}/lib/modules)
MODULESDIR="${TARGET_OUT_STAGING}/lib/modules/${version}"
DESTDIR=$(mktemp -d)

# populate DESTDIR
mkdir -p ${DESTDIR}/lib/modules/${version}
for i in modules.builtin modules.builtin.bin modules.builtin.modinfo modules.order; do
	if [ -f "${MODULESDIR}/${i}" ]; then
		cp -p "${MODULESDIR}/${i}" "${DESTDIR}/lib/modules/${version}/${i}"
	fi
done

export MODPROBE_OPTIONS="-d ${TARGET_OUT_STAGING}"
export MODULESDIR
export BASEDIR=${TARGET_OUT_STAGING}
export FIRMWAREDIR=${TARGET_FIRMWARE_DIR}
export DESTDIR
export version
export verbose

. ${BUILD_SYSTEM}/scripts/driver-and-firmware-functions

# add basic modules
# right now we cannot get the firmwares for the driver selection, so
# we send the output to /dev/null to avoid the errors
case "${TARGET_EARLY_MODULES_CONFIG}" in
most)
	auto_add_modules
	;;
netboot)
	auto_add_modules base
	auto_add_modules net
	;;
*)
	echo "W: unsupported TARGET_EARLY_MODULES_CONFIG: ${TARGET_EARLY_MODULES_CONFIG}." >&2
	echo "W: Falling back to TARGET_EARLY_MODULES_CONFIG = most." >&2
	auto_add_modules /dev/null
	;;
esac

# add modules from $TARGET_VENDOR_DIR/modules
add_modules_from_file "${TARGET_VENDOR_DIR}/modules"

# resolve hidden dependencies
hidden_dep_add_modules

# add firmware for built-in code
add_builtin_firmware

depmod -a -b "${DESTDIR}" "${version}"

# setting mdev conf
echo '$MODALIAS=.*    0:0 0660 */sbin/modprobe -v -b $MODALIAS' > ${TARGET_OUT_FINAL}/etc/mdev.conf

# clean up folders, vars, etc

# remove previous files/folders
rm -rf ${TARGET_OUT_FINAL}/lib/modules/*

# copy all modules to the final folder
cp -p -r ${DESTDIR}${TARGET_OUT_STAGING}/lib/modules/* ${TARGET_OUT_FINAL}/lib/modules/
cp -p -r ${MODULESDIR}/modules.* ${TARGET_OUT_FINAL}/lib/modules/${version}/

# create folder and copy firmware
if mkdir -p ${TARGET_OUT_FINAL}/lib/firmware 2> /dev/null; then
	cp -p -r ${DESTDIR}${TARGET_FIRMWARE_DIR}/* ${TARGET_OUT_FINAL}/lib/firmware/
else
	echo "W: couldn't not create ${TARGET_OUT_FINAL}/lib/firmware no firmware will be included"
fi

# remove the temporal folder
rm -rf ${DESTDIR}

# clean env vars
unset MODPROBE_OPTIONS
unset MODULESDIR
unset DESTDIR
unset BASEDIR
unset FIRMWAREDIR
unset version
unset verbose
